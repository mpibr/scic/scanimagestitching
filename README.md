# ScanImageStitching

ScanImage tiles and z-scans stitching

## Fiji
ImageJ2
version: 2.3.0/1.53f

## Plugins
Grid/Collection Stiching version: 1.2

issues: snake order argument has white space in it

```
^Right & Down                $
```

## Running headless script

```
/Applications/Fiji.app/Contents/MacOS/ImageJ-macosx --ij2 --headless --console --run ScanImageStitching.ijm
```

## ScanImage issue

[issue](https://forum.image.sc/t/converting-16-bit-signed-tiffs-to-16-bit-unsigned/5847/3) signed vs unsigned 16 bit images

## Microscopy Image Stitching Tool (MIST)
[MIST](https://github.com/USNISTGOV/MIST/wiki/Install-Guide)

## Pipeline

* even stack = "right & down"
* odd stack = "right & up"
* alternate based on the stack number

## Run

```
Plugins > Macros > Run ... > ScanImageStitching.ijm
```

## Snake Indexing

### Grid: snake by rows
* Right & Down
* Left & Down
* Right & Up
* Left & Up


### Even Grid

* Z = odd -> {Right & Down}

* Z = even && Grid = even -> {Right & Up}
* Z = even && Grid = odd -> {Left & Up}

