/*
 * ScanImageStitching macro
 *     pre-process images with reverse stack order
 *     Grid/Collection stiching plugin is required
 * version 0.1
 * Dec 2021
 * sciclist@brain.mpg.de
 */


/* check pathname for trailing separator
 * @param: path of interest
 */
function checkPathSeparator(path)
{
    if (!endsWith(path, File.separator))
        path = path + File.separator;
    return path;
}


 /* get all files from direcotry
  * @param: input directory
  * @param: file extension
  */
 function getFileListWithExtension(path, extension)
 {
    listOfFiles = getFileList(path);
    listOfExtension = newArray;
    i = 0;
    for (f = 0; f < listOfFiles.length; f++) {
        fileName = listOfFiles[f];
        if (endsWith(toLowerCase(fileName), extension)) {
            fullFileName = path + fileName;
            listOfExtension = Array.concat(listOfExtension, fullFileName);
            i++;
        }
    }
    return listOfExtension;
}


/* convert signed to unsigned 16bit
 * scales between min and max value
 * requires open stack
 */
function convertSignedToUnsigned16bit()
{
    run("32-bit");
    for (s = 1; s <= nSlices; s++) {
        setSlice(s);
        getStatistics(area, mean, pixelMin, pixelMax);
        run("Subtract...", "value=" + pixelMin);
        setMinAndMax(0, 65535);
    }

    run("16-bit");    
    for (s = 1; s <= nSlices; s++) {
        setSlice(s);
        getStatistics(area, mean, pixelMin, pixelMax);
        setMinAndMax(pixelMin, pixelMax);
    }
}


/* export each image from a stack
 * prepends stack index to filename
 * @param export path
 */
function exportStackToImages(path)
{
    run("Stack to Images");
    for (i = nImages; 0 < i; i--) {
        fileName = getTitle();
        fileTag = substring(fileName, 0, lastIndexOf(fileName, "-"));
        fileOut = path + "S" + i + "_" + fileTag + ".tif";
        saveAs("Tiff", fileOut);
        close();
    }
}


/* get all files from direcotry
  * @param: output directory
  * @param: list of files
  */
function preprocessStacks(path, files)
{
    countSlices = 0;
    for (f = 0; f < files.length; f++) {
        showProgress(f, files.length);
        fileIn = files[f];
        fileName = File.getName(fileIn);
        
        run("Bio-Formats Windowless Importer", "open="+fileIn);
        countSlices = nSlices;
        convertSignedToUnsigned16bit();
        exportStackToImages(path);
        //break;
    }
    return countSlices;
}


/* main macro
 */
macro "ScanImageStitching"
{
    // set directories
    pathInput = getDirectory("choose a directory");
    pathTemp = checkPathSeparator(pathInput + "temp");
    pathStitch = checkPathSeparator(pathInput + "stitched");
    File.makeDirectory(pathTemp);
    File.makeDirectory(pathStitch);
    
    // grab all files for stitching
    filesList = getFileListWithExtension(pathInput, ".tif");

    // set defaults
    gridX = 6;
    gridY = 6;
    gridZ = 5;
    idxStart = 1;
    fileTag="tag_{iiiii}";

    // create input dialog
    Dialog.create("Stitching Arguments");
    Dialog.addNumber("GridX:", gridX);
    Dialog.addNumber("GridY:", gridY);
    Dialog.addNumber("GridZ:", gridZ);
    Dialog.addNumber("Start index:", idxStart);
    Dialog.addString("File name tag:", fileTag);
    Dialog.show();

    // parse arguments
    gridX = Dialog.getNumber();
    gridY = Dialog.getNumber();
    gridZ = Dialog.getNumber();
    idxStart = Dialog.getNumber();
    fileTag = Dialog.getString();
    
    // validate parameters
    if (gridX * gridY * gridZ != filesList.length)
        exit("ScanImageStitching::error, grid size does not match number of files.");

    // pipeline
    setBatchMode(true);
    
    countSlices = preprocessStacks(pathTemp, filesList);

    for (z = 1; z <= gridZ; z++) {

        // set order (this param contains empty space, not a typo!!!)
        // Z = even -> Right & Down
        // Z = odd AND grid = even -> Right & Up
        // Z = odd AND grid = odd -> Left & Up
        order = "Right & Down                ";
        if ((z % 2) == 0) {
            order = "Left & Up";
            if ((gridY % 2) == 0)
                order = "Right & Up";
        }
        
        // stitch
        for (s = 1; s <= countSlices; s++) {

            run("Grid/Collection stitching", 
            "type=[Grid: snake by rows] " +
            "order=[&order] " +
            "grid_size_x=&gridX grid_size_y=&gridY " +
            "tile_overlap=10 " +
            "first_file_index_i=&idxStart " +
            "directory=&pathTemp " +
            "file_names=S" + s + "_" +  fileTag + ".tif " +
            "output_textfile_name=TileConfiguration.txt " +
            "fusion_method=[Linear Blending] " +
            "regression_threshold=0.30 " +
            "max/avg_displacement_threshold=2.50 " +
            "absolute_displacement_threshold=3.50 " +
            "compute_overlap ignore_z_stage display_fusion " +
            "computation_parameters=[Save memory (but be slower)] " +
            "image_output=[Write to disk] " +
            "output_directory=&pathStitch");
            
            stitchFileIn = pathStitch + "img_t1_z1_c1";
            stitchFileOut = pathStitch + "S" + s + "_Z" + z + ".tif";
            File.rename(stitchFileIn, stitchFileOut);
        }
        
        idxStart = idxStart + gridX * gridY;
    }

    setBatchMode(false);
}